var mysql_host = "localhost";
var Sequelize = require('sequelize');
var sequelize = new Sequelize('DailyEvent', 'root', 'Gdl018052015', {
 host: mysql_host,
 dialect: 'mysql'
});

var payments = sequelize.define('payments', {
    "paymentid": { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
    "payerId" : { type: Sequelize.STRING },
	"payerToken" : { type: Sequelize.STRING },
    "paypalCreate": { type: Sequelize.JSON },
    "paypalCreateResponse": { type: Sequelize.JSON },
    "payPalPaymentID": { type: Sequelize.STRING },
    "qty" : { type: Sequelize.STRING },
    "created_at" : { type: Sequelize.DATE },
    "status" : { type: Sequelize.STRING,  defaultValue: Sequelize.NOW }
}); 
payments.sync();
module.exports = payments;