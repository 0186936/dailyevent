var mysql_host = "localhost";
var Sequelize = require('sequelize');
var sequelize = new Sequelize('DailyEvent', 'root', 'Gdl018052015', {
 host: mysql_host,
 dialect: 'mysql'
});


var users = sequelize.define('users', {
   "facebook_code" : {type: Sequelize.TEXT},
    //id de fb y codigo
    "facebookID" : {type: Sequelize.STRING},
    "facebook_access_token" :{type: Sequelize.TEXT},
    "userid": { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
	"email" : { type: Sequelize.STRING },
    "name": { type: Sequelize.STRING },
    
    "username" : { type: Sequelize.STRING },
    "password" : { type: Sequelize.STRING },

    "address" : { type: Sequelize.STRING },
    "city" : { type: Sequelize.INTEGER, allowNull: true },
    "birthday" : { type: Sequelize.DATEONLY, allowNull: true },
    "gender" : { type: Sequelize.CHAR, allowNull: true },
    "school" : { type: Sequelize.STRING, allowNull: true },
    "country" : { type: Sequelize.STRING, allowNull: true },
    "location" : { type: Sequelize.GEOMETRY, allowNull: true },
    "interests" : { type: Sequelize.JSON , allowNull: true},
    "created_at" : { type: Sequelize.DATE },
    "status" : { type: Sequelize.STRING,  defaultValue: Sequelize.NOW }
}); 
users.sync();
module.exports = users;