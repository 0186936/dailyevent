var nodemailer = require('nodemailer');
var hbs = require('nodemailer-express-handlebars');
var options = {
 viewEngine: {
     extname: '.hbs',
     layoutsDir: 'views/email/'
 },
 viewPath: 'views/email/',
 extName: '.hbs'
};
// var sgTransport = require('nodemailer-sendgrid-transport');

var gmail = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'mydailyeventpage@gmail.com',
        pass: 'mydailyevent'
    }
});

// var mailer = nodemailer.createTransport(sgTransport(gmail));
gmail.use('compile', hbs(options));

module.exports = gmail;