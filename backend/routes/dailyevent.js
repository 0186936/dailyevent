var express = require('express');
var router = express.Router();
var rest = require("../mymodules/rest");

var tablaeventos=require("../models/event");
var tablaUsers=require("../models/users");


router.get('/MainPage', function(req, res, next) {	

		if(req.session && req.session.user)
		{
			res.render("MainPage");

		}
		else
		{

			res.redirect('/login');

		}
		
});

module.exports = router;