var express = require('express');
var sha256 = require('sha256');
var router = express.Router();
var paypal = require('paypal-rest-sdk');

var tablaPayment = require("../models/payment");

paypal.configure({
  'mode': 'sandbox', //sandbox or live
  'client_id': 'ATUkEqrt_HkUWF96pd5j-Yi0J0HbfkKXLdrVGMtCeyoIkapmwxuIsr_e-26V5blVFtU1tUvyCdI0S0I3',
  'client_secret': 'EAkazenDOKJ3oleSWmu2WWwTTtWG3vTFrupSBrp3kOcqgj0osbcm72XKdaaHJvEdEWjQOreZ_Ufq4U-C'
});

/* GET home page. */
router.post('/paypal_create', function(req, res, next) {  //recibe una cantidad y se llama en boton de frontend
	console.log(req.body);

	var create_payment_json = {
	    "intent": "sale",
	    "payer": {
	        "payment_method": "paypal"
	    },
	    "redirect_urls": {
	        "return_url": "http://mydailyevent.com/paypal/success", //front end si se efectua pago
	        "cancel_url": "http://mydailyevent.com/paypal/error"  // front si se cancela, ruta de abajo
	    },
	    "transactions": [{	       
	        "amount": {
	            "currency": "MXN",
	            "total": req.body.qty
	        },
	        "description": "This is the payment description."
	    }]
	};

	paypal.payment.create(create_payment_json, function (error, payment) {
	    if (error) {
		  	console.log(error);	        
	    } else {
	        console.log("Create Payment Response");
	        console.log(payment);
			var paymentData = {
				paypalCreate : create_payment_json,
				paypalCreateResponse : payment,
				payPalPaymentID : payment.id,
				qty : req.body.qty,
				status : "created"
			};
			tablaPayment.build(paymentData).save().then(function(){		//guarda transac. en BD
	        	res.redirect(payment.links[1].href)
			}).catch(function(err){
			  	console.log(err);
		    });
	    }
	});


});


router.get('/error', function(req, res, next) { 
	var paymentData = {
		status : "failed"
	};

	tablaPayment.update(paymentData, {   //actualizar en BD
        where: {
            payPalPaymentID : req.query.paymentId
        }
    }).then(function(){

		res.send("Paypal Error");
    });
});

router.get('/success', function(req, res, next) { 
	var paymentData = {
		payerId : req.query.PayerID,
		paypalToken : req.query.token,
		status : "completed"
	};

	tablaPayment.update(paymentData, {   //actualizar en BD
        where: {
            payPalPaymentID : req.query.paymentId
        }
    }).then(function(){

		res.render("Success");
    });


});

module.exports = router;